from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User



class Projects(models.Model):
    name = models.CharField(max_length=50)
    created_date = models.DateField(default=timezone.datetime.now())
    posts_count = models.IntegerField(max_length=None)
    def __str__(self):
        return self.name+" ("+str(self.posts_count)+")"



class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateField(default=timezone.datetime.now())
    published_date = models.DateField(blank=True,null=True)
    project = models.ForeignKey(Projects,default=None)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

class UserProfile(User):
    posts_count = models.IntegerField(max_length=None)
    is_admin = models.BooleanField(default=False)
    registered_date = models.DateField(default=timezone.datetime.now())
    def __str__(self):
        return self.username
