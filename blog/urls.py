from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^index.html',views.post_list, name='post_list'),
    url(r'register.html',views.register),
    url(r'add_post.html',views.add_post)
    ]