from django.shortcuts import render
from django.utils import timezone
from .models import Post


# Create your views here.
def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request, 'blog/index.html',{'posts':posts})
def register(request):
    return render(request, 'blog/register.html',{})
def add_post(request):
    return render(request,'blog/add_post.html',{})
